<?php
define( 'WP_CACHE', true );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "ggk2-db" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DAB&x;EE,SI)?5I#^*2dJ?<OUhFe^puxHW^~$|Aj=LnazlcVQtry^bbaqIZE^d1u' );
define( 'SECURE_AUTH_KEY',  '+#/UIM!ump&b?A%coqR}0I~CI|Xt&,$G~~k!E&c!B*EgS<!rF !j,tgJrb,!O+HL' );
define( 'LOGGED_IN_KEY',    '.=).QG,Wtt%uQNo{2c<0S :+}&#lboUwb9adiD1ibC!e3sc3hkP%KcI4/`=}O1~A' );
define( 'NONCE_KEY',        'R&eU8dm<kROolU_e2Ii)U%/PQ2lGah#t?;mZ/*=ygAZ`M%62:oPt2o:wi[4<-/~^' );
define( 'AUTH_SALT',        '1!@F=ywao)O`f}TU.!dHS=uiPS1!mA~y|s&]ZKTJ|Yy06|]iK*!z,+=ugQ.^TQM_' );
define( 'SECURE_AUTH_SALT', '/cLb/kXg]iNJOEw*Ta O1WXaVc|:gkn*3yA|H&H&][xkEP{@*Wb~MM1cmODJJw~@' );
define( 'LOGGED_IN_SALT',   ')Xm]!q&x)_/RAye=UP:@)hy2z9@d58C9ZYT0@9Xp>rc@J2$}so,i%01YAP=U7GC ' );
define( 'NONCE_SALT',       '$uWg4f>oT;IP6tHi-|>)9y-RYxf3uM)EVs*VCd..0C<[h2c%oPNw>?I3,GUiT^@C' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
